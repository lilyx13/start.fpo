��          �      <      �  {   �  %   -     S     c     z     �  
   �     �     �  !   �     �  7   �     5  V   >  L   �     �  #   �  �    {   �  %        @     P     g     |  
   �     �     �  !   �     �  7   �     "  V   +  L   �     �  #   �                      	                                                                           
    %(year)s Red Hat, Inc. and others. Please send any comments or corrections to the <a href="%(mail_url)s">websites team</a>. A Red Hat-Sponsored Community Project Content License Fedora Code of Conduct Fedora Documentation Fedora Project - Start Page Get Fedora Help for Fedora Users Join us. Latest news from Fedora Magazine: Legal Read more at <br /><strong>fedoramagazine.org</strong>! Sponsors The Fedora Project is maintained and driven by the community and sponsored by Red Hat. This is a community maintained site. Red Hat is not responsible for content. Trademark Guidelines Want to contribute <br />to Fedora? Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-11-03 22:43+0000
PO-Revision-Date: 2021-11-03 22:43+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 %(year)s Red Hat, Inc. and others. Please send any comments or corrections to the <a href="%(mail_url)s">websites team</a>. A Red Hat-Sponsored Community Project Content License Fedora Code of Conduct Fedora Documentation Fedora Project - Start Page Get Fedora Help for Fedora Users Join us. Latest news from Fedora Magazine: Legal Read more at <br /><strong>fedoramagazine.org</strong>! Sponsors The Fedora Project is maintained and driven by the community and sponsored by Red Hat. This is a community maintained site. Red Hat is not responsible for content. Trademark Guidelines Want to contribute <br />to Fedora? 