# README

This readme is particularly for the page revamp. As such I've included the original build notes. However those will be kept at the bottom and I will use this to outline important information about the revamp project.

## Important Frontend Issues

- Restructuring how partials are handled is important
    - some naming conventions are confusing
    - Use of semantic tags 
    - Make sure to keep dynamic dates etc
    - Updating from HTML 4 to 5 regarding xml will require some further reading

## Important Backend Issues

- Creating the Community Blog feed gatherer
- Creating the search bar 

--- 
## Getting started

- Note that you should have already set up the docker image correctly 

**WARNING:** DO NOT execute the following steps as root, just use your normal user:

### 1. Setup your system

    sudo dnf install git gettext python2-genshi python-lxml python-setuptools python-dateutil \
    python-dogpile-cache babel python-feedparser fedfind python-requests python2-babel

    sudo dnf groups install 'Web Server'

### 2. Clone the fedora-websites repo

    git clone https://pagure.io/fedora-websites.git

### 3. Enter the directory of the website you want to work on
For example, if you want to work on getfedora.org, use:

    cd fedora-websites/getfedora.org

### 4. Launch a test instance

    make en test

### 5. Stop the test instance

    make stoptest

**Note:** if you have caching problems you can clean the instance even more:

    make veryclean
